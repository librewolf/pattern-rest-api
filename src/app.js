const OS = require('os')
const CS = require('chalk')
const cluster = require('cluster')
const Incs = require('./incs')
const { winInfo, winErr } = require('./util/logger')
const { app: expressApp } = require('./config/express')

class App extends Incs {
	constructor(worker, autoScale = true) {
		super()
		this.PORT = process.env.APP_PORT || 5001
		this.createCluster(worker, autoScale)
	}

	createCluster = (worker, autoScale) => {
		if (autoScale) worker = this.autoScale(worker)

		if (cluster.isMaster) {
			for (let i = 0; i < worker; i += 1) {
				winInfo(`****** Creating instances [ ${worker} ] ******`)
				cluster.fork()
			}

			cluster.on('exit', worker => {
				winErr(`****** Worker offline [ ${worker.id} ] ******`)
				cluster.fork()
			})
		} else
			expressApp.listen(this.PORT, async () => {
				await this.buildMongoose()
				console.info(
					CS.green(`Server started and running on port [ ${this.PORT} ]`)
				)
			})
	}

	autoScale = worker => {
		const cpus = OS.cpus().length
		return cpus / worker
	}
}

new App(1, false)
