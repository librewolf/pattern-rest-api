const winston = require('winston')

const loggers = {
	error: winston.createLogger({
		level: 'error',
		format: winston.format.combine(winston.format.timestamp()),
		transports: [new winston.transports.Console()]
	}),
	info: winston.createLogger({
		level: 'info',
		format: winston.format.combine(
			winston.format.json(),
			winston.format.timestamp(),
			winston.format.ms()
			// winston.format.prettyPrint()
		),
		transports: [new winston.transports.Console()]
	})
}

const win = {
	winInfo: loggers.info.info,
	winErr: loggers.error.error
}

module.exports = { ...win }
