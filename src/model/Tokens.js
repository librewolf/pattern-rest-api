const { Schema, model } = require('mongoose')

const SchemaTokens = new Schema({
	name: {
		type: String,
		required: true
	},
	token: {
		type: String,
		required: true,
		unique: true
	},
	date: {
		type: Number,
		default: Date.now
	}
})

module.exports = model('Token', SchemaTokens)
