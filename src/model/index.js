const requireModel = {
	Token: require('./Tokens')
}

const db = {}

Object.keys(requireModel).forEach(model => {
	// eslint-disable-next-line security/detect-object-injection
	return (db[model] = requireModel[model])
})

module.exports = { db }
