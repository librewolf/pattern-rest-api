const { db } = require('../../model')
const { check, validationResult } = require('express-validator')

class HubColl {
	async createToken(req, res) {
		const isErr = validationResult(req)
		const { token: getToken, name: getName } = req.body

		if (!isErr.isEmpty())
			return res.status(400).json({
				success: false,
				error: isErr.array(),
				message: `Error validation [ ${res.statusCode} ]`
			})

		const create = new db.Token({
			name: getName,
			token: getToken
		})

		return await create
			.save()
			.then(() => res.status(200).json({ success: true, message: create }))
			.catch(err => {
				res.status(400).json({
					success: false,
					message: `Duplicate key error [ ${err.message.split('"')[1]} ]`
				})
				console.log(err.message)
			})
	}

	get hasValidator() {
		return [
			check('token', 'Token len: min 10 or 30 max').isLength({
				min: 10,
				max: 32
			})
		]
	}
}

module.exports = new HubColl()
