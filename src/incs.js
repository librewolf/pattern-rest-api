const CS = require('chalk')
const mongoose = require('mongoose')
const { config } = require('./config/config')

class Incs {
	buildMongoose = async () => {
		await mongoose
			.connect(process.env.MONGO_URL, {
				keepAlive: true,
				useNewUrlParser: true,
				useUnifiedTopology: true,
				useCreateIndex: true,
				socketTimeoutMS: 10000,
				connectTimeoutMS: 30000
			})
			.then(() =>
				console.info(
					CS.green(`Mongoose connected to => [ ${process.env.MONGO_URL} ]`)
				)
			)
			.catch(err => console.error(CS.red(`Error mongoose: ${err.message}`)))

		mongoose.set('debug', config.isDev)

		process.on('SIGINT', () => {
			mongoose.connection.close(() => {
				console.info(CS.red('Close connection mongoose'))
				process.exit(0)
			})
		})
	}
}

module.exports = Incs
