const ms = require('ms')

const config = {}

config.isDev = process.env.NODE_ENV === 'development'

config.rateLimitOpts = {
	windowMs: ms('5m'),
	max: process.env.RATELIMIT_MAX,
	message: 'Rate limit exceeded, please try again after an 5min !'
}

module.exports = { config }
