const express = require('express')
const rateLimit = require('express-rate-limit')
const boyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const morgan = require('morgan')
const consign = require('consign')
const Waf = require('mini-waf/wafbase')
const wafrules = require('mini-waf/wafrules')
const { config } = require('../config/config')
// const { errorMiddle } = require('../middle/index')

const app = express()

if (!config.isDev) {
	app.use(rateLimit(config.rateLimitOpts))
	app.use(Waf.WafMiddleware(wafrules.DefaultSettings))
}

app.use(boyParser.json())
app.use(boyParser.urlencoded({ extended: false }))
app.use(morgan('dev'))
app.use(cors())
app.use(compression())
app.use(helmet())

consign({
	cwd: process.cwd() + '/src',
	extensions: ['.js', '.json'],
	verbose: false
})
	.include('coll/v1')
	.then('middle')
	.then('route/v1')
	.into(app)

app.all('*', (req, res) => {
	return res.status(404).json({ error: 'Page not found !' })
})

module.exports = { app }
