module.exports = app => {
	const { HubColl } = app.coll.v1

	app.post('/v1/token', HubColl.hasValidator, HubColl.createToken)
}
